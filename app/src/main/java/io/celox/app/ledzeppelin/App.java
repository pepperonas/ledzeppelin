package io.celox.app.ledzeppelin;

import android.app.Application;

import com.pepperonas.aespreferences.AesPrefs;
import com.pepperonas.andbasx.AndBasx;

import java.util.Date;

import io.celox.app.ledzeppelin.utils.AesConst;
import io.celox.app.ledzeppelin.utils.Log;
import io.celox.app.ledzeppelin.utils.Utils;

/**
 * @author Martin Pfeffer <a href="mailto:martin.pfeffer@celox.io">martin.pfeffer@celox.io</a>
 * @see <a href="https://celox.io">https://celox.io</a>
 */
public class App extends Application {

    private static final String TAG = "App";

    @Override
    public void onCreate() {
        super.onCreate();

        AesPrefs.init(this, AesConst.AES_PREFS_FILE_NAME,
                "XgLfNEIn@E2uVUg69E*wQ!sgsBLO&S$UYEZ", AesPrefs.LogMode.ALL);

        Date d = new Date();
        d.setTime(System.currentTimeMillis() - 100000);

        AndBasx.init(this);
        AesPrefs.initInstallationDate();
        AesPrefs.initOrIncrementLaunchCounter();

        AesPrefs.putLong(AesConst.APP_STARTED, System.currentTimeMillis());

        Log.i(TAG, "onCreate: App started... " + Utils.getReadableTimeStamp(AesPrefs.getLong(AesConst.APP_STARTED, System.currentTimeMillis())));
    }
}

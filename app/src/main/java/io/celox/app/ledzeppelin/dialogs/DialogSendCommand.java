package io.celox.app.ledzeppelin.dialogs;

import android.app.AlertDialog;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.pepperonas.materialdialog.MaterialDialog;

import io.celox.app.ledzeppelin.DeviceControlActivity;
import io.celox.app.ledzeppelin.R;

/**
 * @author Martin Pfeffer <a href="mailto:martin.pfeffer@celox.io">martin.pfeffer@celox.io</a>
 * @see <a href="https://celox.io">https://celox.io</a>
 */
public class DialogSendCommand {

    @SuppressWarnings("unused")
    private static final String TAG = "DialogSendCommand";

    public DialogSendCommand(final DeviceControlActivity deviceControlActivity) {
        new MaterialDialog.Builder(deviceControlActivity)
                .title(R.string.send_command)
                .customView(R.layout.dialog_send_command)
                .showListener(new MaterialDialog.ShowListener() {
                    @Override
                    public void onShow(final AlertDialog dialog) {
                        super.onShow(dialog);
                        dialog.findViewById(R.id.btn_send_command).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                TextView tvCommand = dialog.findViewById(R.id.tv_command);
                                if (tvCommand.getText() != null && !tvCommand.getText().toString().isEmpty()) {
                                    Intent sendCommand = new Intent("send_command");
                                    sendCommand.putExtra("command", tvCommand.getText().toString());
                                    deviceControlActivity.sendBroadcast(sendCommand);
                                }
                            }
                        });
                    }
                })
                .show();
    }
}

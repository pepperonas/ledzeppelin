package io.celox.app.ledzeppelin.utils;

/**
 * @author Martin Pfeffer <a href="mailto:martin.pfeffer@celox.io">martin.pfeffer@celox.io</a>
 * @see <a href="https://celox.io">https://celox.io</a>
 */
public class AesConst {

    public static final String AES_PREFS_FILE_NAME = "aes_prefs";
    public static final String APP_STARTED = "app_started";
    public static final String PERMISSIONS_GRANTED = "permissions_granted";

    public static final String SERVICE_UUIDS = "service_uuids";
    public static final String CHARACTERISTICS_UUIDS = "characteristics_uuids";
}

package io.celox.app.ledzeppelin.utils;

import java.util.HashMap;

/**
 * @author Martin Pfeffer <a href="mailto:martin.pfeffer@celox.io">martin.pfeffer@celox.io</a>
 * @see <a href="https://celox.io">https://celox.io</a>
 */
public class Const {

    private static final String TAG = "Const";

    public static String CUSTOM_SERVICE_LED = "0000ffe5-0000-1000-8000-00805f9b34fb";
    public static String CHARACTERISTIC_LED = "0000ffe9-0000-1000-8000-00805f9b34fb";

    private static HashMap<String, String> attributes = new HashMap<>();

    static {
        attributes.put(CUSTOM_SERVICE_LED, CHARACTERISTIC_LED);
        attributes.put(CHARACTERISTIC_LED, "LED");
    }

    public static String lookup(String uuid, String defaultName) {
        Log.i(TAG, "lookup: uuid=" + uuid + " | defaultName=" + defaultName);
        String name = attributes.get(uuid);
        return name == null ? defaultName : name;
    }

    static final String IMEI_NOT_FOUND = "IMEI_NOT_FOUND";

    static final String LOG_DIR = "ledzeppelin";
    static final String CRASH_DIR = "crashes";
}
